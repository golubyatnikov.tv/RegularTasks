﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using RegularTasks.Models;

namespace RegularTasks.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IWebHostEnvironment _env;

        public HomeController(
            ILogger<HomeController> logger,
            IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("/tests/{id}")]
        public IActionResult GetTestPage(int id)
        {
            return View("TestPage", id);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    [Route("api/tests")]
    public class TestsController : Controller
    {
        private readonly IWebHostEnvironment _env;

        public TestsController(IWebHostEnvironment env)
        {
            _env = env;
        }

        /*[HttpGet("")]
        public IActionResult GetAll()
        {

        }*/

        [HttpGet("{id}")]
        public IActionResult GetTest(int id)
        {
            var testDataPath = _env.ContentRootFileProvider.GetFileInfo($"TestData/{id}.json").PhysicalPath;

            var testData = System.IO.File.ReadAllText(testDataPath);

            return Content(testData, "application/json");
        }

        /*[HttpDelete("{id}")]
        public IActionResult DeteTest(int id)
        {

        }*/
    }
}
