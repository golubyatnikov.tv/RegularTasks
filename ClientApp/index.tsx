import React, { Profiler, useState } from 'react'
import { render } from 'react-dom'
import { TestViewer } from './TestViewer'

const App = () => {
    return <Profiler id='app' onRender={(...args)=>console.log(args)}>
        <div className='container'>
            <TestViewer />
        </div>
    </Profiler>
}

render(<App />, document.getElementById('app-root'));