import React, { Profiler, useEffect, useState } from 'react'
import 'papercss/dist/paper.css'
import ReactMarkdown from 'react-markdown'
import classNames from 'classnames'
import axios from 'axios'

type TestCase = {
  value: string;
  assert: 'match' | 'unmatch'
};

type Test = {
  id: number;
  title: string;
  description: string;
  originalUrl: string;
} & ({
  type: 'testCasesList',
  testCases: Array<TestCase>
}/* | {
  type: 'testCasesText';
  testCases: string;
}*/)

const TestViewer = () => {
  const [test, setTest] = useState<Test>();

  const fetchTest = async () => {
    const testId = (window as any).testId;
    try {
      const res = await axios.get<Test>(`/api/tests/${testId}`)
      setTest(res.data);
    }
    catch (ex) {
      alert(ex);
    }
  }

  useEffect(() => {
    fetchTest()
  }, [])

  const [pattern, setPattern] = useState(''); // ^[+-]?\d+$
  const [testResults, setTestResults] = useState<boolean[]>();

  useEffect(() => {
    if (!pattern) {
      setTestResults(null);
      return;
    }

    let regex: RegExp;
    try {
      regex = new RegExp(pattern);
    }
    catch (err) {
      setTestResults(null);
      return;
    }

    //if (test.type === 'testCasesList') {
    const results = test.testCases.map(c => {
      const m = c.value.match(regex);
      const actualValue = m && m[0] == c.value;
      const expectedValue = c.assert == 'match';
      const result = actualValue == expectedValue;
      return result;
    })
    // }
    // else {
    //   //test.testCases
    // }

    setTestResults(results);
  }, [pattern])

  if (!test)
    return <p>Загрузка...</p>

  return <article className="article">
    <h3
      className="article-title margin-botton-small"
      style={{ marginBottom: 5 }}>
      <p style={{ margin: 0, padding: 0 }}>
        {test.title}
      </p>
    </h3>
    <p className="article-meta">{test.originalUrl}</p>
    <ReactMarkdown className="text-lead" source={test.description} escapeHtml={false} />

    <input type='text' value={pattern} onChange={e => {
      setPattern(e.target.value);
    }} />

    <Profiler id='testCases' onRender={(...args) => console.log(args)}>
      <div className='margin-small'>
        {test.testCases.map((c, i) => {
          const success = testResults?.[i];
          return <TestCaseItem key={i} success={success} executed={!!testResults} testCase={c} />;
        })}
      </div>
    </Profiler>
  </article>
}

const TestCaseItem: React.FC<{
  success: boolean;
  executed: boolean;
  testCase: TestCase;
}> = React.memo(({success, executed, testCase}) => {
  return <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'baseline' }}>
    <div className='alert padding-small'>{testCase.value}</div>
    <div className='margin-small'>{'>>'}</div>
    <div className={classNames('alert padding-small', {
      'alert-success': executed && success,
      'alert-danger': executed && !success
    })}>{testCase.assert == 'match' ? 'Должно совпадать' : 'Должно не совпадать'}</div>
  </div>
})

export { TestViewer }